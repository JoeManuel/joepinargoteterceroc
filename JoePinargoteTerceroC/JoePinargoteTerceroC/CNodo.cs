﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JoePinargoteTerceroC
{
    public class CNodo
    {

        private int dato;
        private CNodo siguiente;

        public CNodo()
        {
            siguiente = null;
        }

        public int Dato
        {
            get
            {
                return dato;
            }
            set
            {
                dato = value;
            }
        }
        public CNodo Siguiente
        {
            get
            {
                return siguiente;
            }
            set
            {
                siguiente = value;
            }
        }

    }

}
