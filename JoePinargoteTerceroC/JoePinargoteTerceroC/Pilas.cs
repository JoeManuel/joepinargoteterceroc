﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JoePinargoteTerceroC
{
    public class Pilas
    {
        private static int MAX_LENGTH;
        private String[] pila ;
        private int cima ;
        private String[] pilaaux ;
        private int cimaaux;

        public Pilas()
        {
            MAX_LENGTH = 5;
            pila = new String[MAX_LENGTH];
            cima = -1;
            pilaaux = new String[MAX_LENGTH];
            cimaaux = -1;
         }


        public void Insertar()
        {
            Console.WriteLine("Digite un dato para la pila ");
            String entra = Console.ReadLine();

            Apilar(entra);
        }

        public void Apilar(String dato)
        {
            if ((pila.Length - 1) == cima)
            {
                Console.WriteLine( "Capacidad de la pila #1 al limite");
                Imprimir();
            }
            else
            {
                cima++;

                //Console.WriteLine( "Cima 1 en la posición " + cima);
                pila[cima] = dato;

            }
        }

        public void Apilaraux(String dato)
        {
            if ((pilaaux.Length - 1) == cimaaux)
            {
                Console.WriteLine( "Capacidad de la pila auxiliar #1 al limite");
            }
            else
            {
                cimaaux++;
                pilaaux[cimaaux] = dato;
            }
        }

        public bool vaciaaux()
        {
            return (cimaaux == -1);
        }

        public bool vacia()
        {
            if (cima == -1)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        public void Imprimir()
        {
            String quedata, salida = " ";
            if (cima != -1)
            {
                do
                {
                    quedata = Desapilar();
                    salida = salida + quedata + "\n";

                    Apilaraux(quedata);
                } while (cima != -1);
                do
                {
                    quedata = Desapilaraux();
                    Apilar(quedata);
                } while (cimaaux != -1);
                Console.WriteLine( salida);
            }
            else
            {
                Console.WriteLine( "La pila #1 esta vacía");
            }
        }

        public String Desapilar()
        {
            String quedato;
            if (vacia())
            {
                Console.WriteLine( "No se puede eliminar, pila #1 vacía !!!");
                return ("");
            }
            else
            {
                quedato = pila[cima];
                pila[cima] = null;
                --cima;
                return (quedato);
            }
        }

        public String Desapilaraux()
        {
            String quedato;
            if (cimaaux == -1)
            {
                Console.WriteLine( "No se puede eliminar, pila #1 vacía !!!");
                return ("");
            }
            else
            {
                quedato = pilaaux[cimaaux];
                pilaaux[cimaaux] = null;
                --cimaaux;
                return (quedato);
            }
        }

        public void Buscar()
        {
            if (vacia())
            {
                Console.WriteLine( "La pila #1 esta vacìa");
            }
            else
            {
                Console.WriteLine("Digite la cadena a buscar: ");
                String cad = Console.ReadLine();
                String quedata;
                int bandera = 0;
                do
                {
                    quedata = Desapilar();
                    if (cad.Equals(quedata))
                    {
                        bandera = 1;
                    }
                    Apilaraux(quedata);
                } while (cima != -1);
                do
                {
                    quedata = Desapilaraux();
                    Apilar(quedata);
                } while (cimaaux != -1);
                if (bandera == 1)
                {
                    Console.WriteLine( "Elemento encontrado");
                }
                else
                {
                    Console.WriteLine( "Elemento no encontrado :(");
                }
            }
        }
        public int contar()
        {
            String quedata;
            int contador = 0;
            if (cima != -1)
            {
                do
                {
                    quedata = Desapilar();
                    contador = contador + 1;
                    Apilaraux(quedata);
                } while (cima != -1);
                do
                {
                    quedata = Desapilaraux();
                    Apilar(quedata);
                } while (cimaaux != -1);
                Console.WriteLine( "Elementos en la pila #1 : " + contador);
            }
            else
            {
                Console.WriteLine( "La pila #1 esta vacìa");
            }
            return contador;

        }


    }

}
